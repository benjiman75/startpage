/* © Copyright Benjiman Cameron Law 2016 All Rights Reserved */
function defineTitle() {
	var titlleToDisp = readCookie("greetingCookie");
	titlleToDisp = (decodeURI(titlleToDisp));
	document.getElementById("greetingMessageTitle").innerHTML = titlleToDisp;	
}
setInterval(defineTitle, 1000);

/* From External Source */
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
/* End External Source */