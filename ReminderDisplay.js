/* © Copyright Benjiman Cameron Law 2016 All Rights Reserved */
/* Create A Reminder */
function createReminder(){
	reminders = prompt("Enter New Reminder", "Mow the Lawn");
	dataToAddToCookie = ("|" + (reminders));
	if (readCookie("remindersCookie") === null){
		createCookie("remindersCookie", dataToAddToCookie);
	}
	else {
		oldRemindersCookie = (decodeURI(readCookie("remindersCookie")));
		newRemindersCookieData = ("|" + (oldRemindersCookie) + (dataToAddToCookie));
		createCookie("remindersCookie", newRemindersCookieData);
	}
	
}
function remindersToTable(dataToTable){
	var reminder = remindersCookieString.split('|');
	var table = "<table>";
	for (var i=0; i< reminder.length; i++) {
		table = table + "<tr><td>"+ reminder[i]+"</td></tr>"; 
	}
	table = table + "</table>";

	return table;

}
function checkForReminders() {
	remindersCookieString = decodeURI(readCookie("remindersCookie"));
	if (readCookie("remindersCookie") === null) {
		 document.getElementById("remindersHtmlId").innerHTML = ("<table><tr><td>Reminders</td></tr><tr><td>Empty</td></tr></table>");
	}
	else {
		 document.getElementById("remindersHtmlId").innerHTML = (remindersToTable(remindersCookieString));
	}
	
}
setInterval(checkForReminders, 1000);
	
	






/* From External Source */
function getCookie(name) {
  var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
  var result = regexp.exec(document.cookie);
  return (result === null) ? null : result[1];
}

function deleteCookie(name, path, domain) {
  // If the cookie exists
  if (getCookie(name))
    createCookie(name, "", -1, path, domain);
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
/* End Exernal Source */