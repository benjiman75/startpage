/* © Copyright Benjiman Cameron Law 2016 All Rights Reserved */
function getTime() {
	var pastTwelve = false;
	var meridian = "am";
	var currentHour = new Date().getHours();
	var currentMinuite = new Date().getMinutes();
	if (currentMinuite < 10) {
		currentMinuite = ("0" + currentMinuite);
	}
	if (currentHour >= 12) {
		pastTwelve = true;
		meridian = "pm";
		greet_spec = "Good Afternoon";
		currentHour -= 12;
		if (currentHour > 5) {
			greet_spec = "Good Evening";
			if (currentHour > 12) {
				greet_spec = "Good Night";
			}
		}
	}
	if (currentHour < 12 && pastTwelve == false) {
		greet_spec = "Hello";
		if (currentHour < 5) {
			greet_spec = "Good Night";
		}
		if (currentHour < 7 && currentHour > 5) {
			greet_spec = "Wide Awake";
		}
		if (currentHour < 11 && currentHour >= 7) {
			greet_spec = "Good Morning";
		}
	}
	var time = (currentHour + ":" + currentMinuite + " " + meridian);
	 
	createCookie("greetingCookie",greet_spec);
	document.getElementById("currentTime").innerHTML = time;
}
setInterval(getTime, 1000);
/* END COPYRIGHT*/

/* !EXTERNALLY SOURCED CONTENT! NOT INCLUDED IN COPYRIGHT */

function createCookie(name, value, expires, path, domain) {
  var cookie = name + "=" + escape(value) + ";";

  if (expires) {
    // If it's a date
    if(expires instanceof Date) {
      // If it isn't a valid date
      if (isNaN(expires.getTime()))
       expires = new Date();
    }
    else
      expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);

    cookie += "expires=" + expires.toGMTString() + ";";
  }

  if (path)
    cookie += "path=" + path + ";";
  if (domain)
    cookie += "domain=" + domain + ";";

  document.cookie = cookie;

}


function getCookie(name) {
  var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
  var result = regexp.exec(document.cookie);
  return (result === null) ? null : result[1];
}

function deleteCookie(name, path, domain) {
  // If the cookie exists
  if (getCookie(name))
    createCookie(name, "", -1, path, domain);
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
/* !END EXTERNALLY SOURCED CONTENT! */